'use strict';

const router = require('express').Router();
const Controller = require('../controllers/sample');


/**
 * GET /sample/db
 * Sample route
 */
router.get('/db', Controller.getSampleDataFromDB);


/**
 * GET /sample/service
 * Sample route
 */
router.get('/service', Controller.getSampleDataFromService);


module.exports = router;
