'use strict';

const wrap = require('co-express');
const Models = require('../../models');
const Service = require('../services').SampleService;

module.exports = {

	getSampleDataFromDB: wrap(function * (req, res, next) {
		
		try {

			let data = yield GetSampleDataFromDB();
			return res.json(data);

		} catch(error) {
			
			console.log(error);
			res.status(500).send(error);
			
			return next();

		}

	}),

	getSampleDataFromService: wrap(function * (req, res, next) {
		
		try {

			let data = yield GetSampleDataFromService();
			return res.json(data);

		} catch(error) {
			
			console.log(error);
			res.status(500).send(error);
			
			return next();

		}

	})

}


/**
 * Get sample data from database
 * @return {array} Collection of objects
 */
function GetSampleDataFromDB () {
	return new Promise((resolve, reject) => {

		let data = [];

		return resolve(data);

	});
}


/**
 * Get sample data from database
 * @return {array} Collection of objects
 */
function GetSampleDataFromService () {
	return new Promise((resolve, reject) => {

		Service.getPeople()
			.then(function (response) {
				
				return resolve(response);

			})
			.catch(function (error) {

				console.log(error);
				return reject(error);

			});

	});
}
