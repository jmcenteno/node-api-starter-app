'use strict';

const http = require('superagent');

class StarWarsAPI {

	constructor () {

		this.baseUrl = 'http://swapi.co/api';

	}

	getPeople () {
		return new Promise((resolve, reject) => {

	    http.get(this.baseUrl + '/people')
	      .end(function (err, res) {
	        
	        if (err) {

	        	reject(err);

	        } else {

	        	resolve(JSON.parse(res.text));

	        }

	      });

	  });
	}

	getPerson (id) {
		return new Promise((resolve, reject) => {

	    http.get(this.baseUrl + '/people/${id}')
	      .end(function (err, res) {
	        
	        if (err) {

	        	reject(err);

	        } else {

	        	resolve(res);

	        }

	      });

	  });
	}

	getPlanets (id) {
		return new Promise((resolve, reject) => {

	    http.get(this.baseUrl + '/planets')
	      .end(function (err, res) {
	        
	        if (err) {

	        	reject(err);

	        } else {

	        	resolve(res);

	        }

	      });

	  });	
	}

}

module.exports = new StarWarsAPI();
